/*
** String.c for my_string in /home/gravie_j/Documents/projets/piscine/piscine_cpp_d03/ex00
** 
** Made by Jean Gravier
** Login   <gravie_j@epitech.net>
** 
** Started on  Fri Jan 10 11:09:37 2014 Jean Gravier
** Last update Fri Jan 10 12:52:01 2014 Jean Gravier
*/

#include <stdlib.h>
#include <string.h>
#include "String.h"

void	StringInit(String* this, char const * s)
{
  this->str = strdup(s);
}

void	StringDestroy(String* this)
{
  free(this->str);
}
