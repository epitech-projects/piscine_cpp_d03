/*
** String.c for my_string in /home/gravie_j/Documents/projets/piscine/piscine_cpp_d03/ex00
** 
** Made by Jean Gravier
** Login   <gravie_j@epitech.net>
** 
** Started on  Fri Jan 10 11:09:37 2014 Jean Gravier
** Last update Fri Jan 10 23:35:37 2014 Jean Gravier
*/

#include <stdlib.h>
#include <string.h>
#include "String.h"

static	void	assign_s(String *this, String const * str);
static	void	assign_c(String *this, char const * s);

void	StringInit(String* this, char const * s)
{
  this->str = strdup(s);
  this->assign_s = &assign_s;
  this->assign_c = &assign_c;
}

void	StringDestroy(String* this)
{
  free(this->str);
  this->str = NULL;
  this->assign_s = NULL;
  this->assign_c = NULL;
  this = NULL;
}

static	void	assign_s(String *this, String const * str)
{
  StringInit(this, str->str);
  this->assign_s = str->assign_s;
  this->assign_c = str->assign_c;
}

static	void	assign_c(String *this, char const * s )
{
  this->str = strdup(s);
}
