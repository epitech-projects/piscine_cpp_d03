/*
** String.h for my_string in /home/gravie_j/Documents/projets/piscine/piscine_cpp_d03/ex00
** 
** Made by Jean Gravier
** Login   <gravie_j@epitech.net>
** 
** Started on  Fri Jan 10 11:09:29 2014 Jean Gravier
** Last update Sat Jan 11 01:26:21 2014 Jean Gravier
*/

#ifndef STRING_H_
# define STRING_H_

typedef	struct	s_String
{
  char		*str;
  void		(*assign_s)(struct	s_String *, struct s_String const *);
  void		(*assign_c)(struct s_String *, char const *);
  void		(*append_s)(struct s_String* this, struct s_String const* ap);
  void		(*append_c)(struct s_String* this, char const* a);
}		String;

void	StringInit(String* this, char const * s);
void	StringDestroy(String* this);

#endif /* !STRING_H_ */
