/*
** String.c for my_string in /home/gravie_j/Documents/projets/piscine/piscine_cpp_d03/ex02
** 
** Made by Jean Gravier
** Login   <gravie_j@epitech.net>
** 
** Started on  Sat Jan 11 00:22:41 2014 Jean Gravier
** Last update Sat Jan 11 03:42:41 2014 Jean Gravier
*/

#include <stdlib.h>
#include <string.h>
#include "String.h"

static	void	assign_s(String *this, String const * str);
static	void	assign_c(String *this, char const * s);
static	void	append_s(String* this, String const* ap);
static	void	append_c(String* this, char const* ap);
static	char	at(String* this, size_t pos);
static	void	clear(String* this);
static	int	size(String* this);

void	StringInit(String* this, char const * s)
{
  this->str = strdup(s);
  this->assign_s = &assign_s;
  this->assign_c = &assign_c;
  this->append_s = &append_s;
  this->append_c = &append_c;
  this->at = &at;
  this->clear = &clear;
  this->size = &size;
}

void	StringDestroy(String* this)
{

  if (this->str != NULL)
    free(this->str);

  this->str = NULL;
  this->assign_s = NULL;
  this->assign_c = NULL;
  this->append_s = NULL;
  this->append_c = NULL;
  this->at = NULL;
  this->clear = NULL;
  this->size = NULL;
  this = NULL;
}

static	void	assign_s(String *this, String const * str)
{
  this->str = strdup(str->str);
  this->assign_s = str->assign_s;
  this->assign_c = str->assign_c;
  this->append_s = str->append_s;
  this->append_c = str->append_c;
  this->at = str->at;
  this->clear = str->clear;
  this->size = str->size;
}

static	void	assign_c(String *this, char const * s)
{
  this->str = strdup(s);
}

static	void	append_s(String* this, String const* ap)
{
  char	*temp;

  temp = malloc(strlen(this->str) + strlen(ap->str));
  temp = strcat(temp, ap->str);
  this->str = temp;
}

static	void	append_c(String* this, char const* ap)
{
  char	*temp;

  temp = malloc(strlen(this->str) + strlen(ap));
  temp = strcat(temp, ap);
  this->str = temp;
}

static	char	at(String* this, size_t pos)
{
  if (pos >= strlen(this->str))
    return (-1);
  return (this->str[pos]);
}

static	void	clear(String* this)
{
  if (this->str != NULL)
    free(this->str);

  this->str = NULL;
  this->assign_s = NULL;
  this->assign_c = NULL;
  this->append_s = NULL;
  this->append_c = NULL;
  this->at = NULL;
  this->clear = NULL;
  this->size = NULL;
}

static	int	size(String* this)
{
  if (this != NULL)
    {
      if (this->str != NULL)
	return (strlen(this->str));
      else
	return (-1);
    }
  return (-1);
      
}
